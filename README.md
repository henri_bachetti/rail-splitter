# RAIL-SPLITTER

The purpose of this page is to explain step by step the realization of a rail splitter delivering 2x2.5V to 2x18V.

It has a switch to enable / disable the output. A red LED indicates that the output is active.

The power supply uses the following components :

 * an OPA134
 * a pair of darlington TIP120 / TIP125
 * some passive components

### ELECTRONICS

The schema is made using KICAD.

### BLOG
A description in french here : 

